+++
title = "Containerize a CLI application"
weight = 10
chapter = true
pre = "<b>2. </b>"
+++
<br />
In this module, we will first containerize a simple Windows CLI application. We'll then run it locally and make additional changes to the container image.


__Optional__:
For more details, please watch the video https://youtu.be/ImWFA37QEt8

1. Open the [EC2 web console](https://console.aws.amazon.com/ec2/home?region=us-east-1#Instances:sort=instanceState) and select the lab machine.

1. Make a note of the __Public DNS__ or the __Public IP__ of the lab machine.

1. Open your __RDP__ client and connect to the lab machine using the DNS or IP you got from the previous step.

    __Note:__ If you are using a Mac and don't have an RDP client, please download one. You can find the login credential in [AWS Secrets Manager](https://console.aws.amazon.com/secretsmanager/home?region=us-east-1#/secret?name=windows-container-host-password). Click the button __Retrieve secret value__. If you have a default domain in the RDP client, you can add a backslash to the user name, for example _\Administrator_.

1. Open a powershell window.

1. Run __docker info__ to view the client/server info.

1. Run __docker images__ and you can see that a few of images have already pulled down to the lab machines.

1. Run __docker ps__ to verify there is no container running.

1. Run __hostname__ to view the current hostname of the EC2 instance.

1. Run the following command to start a container:

```bash
docker run -it mcr.microsoft.com/windows/servercore:ltsc2019 powershell
```

1. The command above will open a new powershell window in the container.

1. Run __hostname__ again and you can see it's different now since we are inside the container.

1. Make a new directory by running the command __mkdir myapp__

1. Open a new powershell window on the windows lab machine.

1. Run __docker ps__ to verify there is now one container running and make a note of the __container id__.

1. Change directory and copy the file to the container (__replace the container id__):

```bash
 cd C:\Users\Administrator\Downloads\assets\140_docker_basic_package_application

 docker cp MyBatchApp.exe {cotainer_id}:c:\myapp
 ```

1. Stop the container, change the entrypoint, and commit the changes.

```bash
docker stop {container_id}

docker commit --change 'EntryPoint "C:\myapp\MyBatchApp.exe"' {container_id} mybatchapp:latest
```

1. Run __docker images__ to verify now we have the image mybatchapp with the tag __latest__.

1. Run the container to verify the change we have commited:

```bash
docker run -it mybatchapp:latest
```

1. You should be able to have output similar to the following:

    <pre>
    worker 0:22:40:44-->14.142135623731
    worker 0:22:40:44-->26.8328157299975
    worker 0:22:40:45-->14.8996644257513
    worker 0:22:40:45-->26.9072480941474
    worker 0:22:40:46-->15.0332963783729
    </pre>

1. You can use __ctrl-c__ or __docker stop {container_id__} to stop the running container.

