+++
title = ""
+++

<h1>Amazon ECS Workshop</h1>
<h2 style='text-align: center;'>MarkLogic</h2>
<br /><br /><br /><br />
Welcome to Amazon ECS workshop using Windows Containers ! 

In this workshop, you get hands-on experience running Windows containers on AWS with [Amazon Elastic Container Service](Amazon Elastic Container Service)

This workshop is based on __Microsoft Containers on AWS Ninja Bootcamp__, which has more labs and runs much longer.

You can find its lab guide at: http://bit.ly/wincon-guide-v-1-2

You can also watch all lab videos at: https://www.youtube.com/playlist?list=PLDvuRFHx48vAUKebX5sQa2nYSTeUS2Mh5
****
Presentation slides for the workshop:

<a href="../../deck/AWSWindowsContainers.pdf">Amazon ECS using Windows Containers</a>

