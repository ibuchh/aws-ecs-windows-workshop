+++
title = "Run a task on ECS"
weight = 15
chapter = true
pre = "<b>3. </b>"
+++
<br />
In this module, you will create a docker image repository and run a simple application as a task on ECS.


### Create an ECR repository

1. Go to the [Amazon ECR](https://us-east-1.console.aws.amazon.com/ecr/repositories?region=us-east-1#) console and click on ***Create repository***. (If it's the first time, click the button __Get Started__)
{{% notice info %}} <b>What is Amazon ECR?</b>
Amazon Elastic Container Registry (ECR) is a fully-managed Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images. Amazon ECR is integrated with Amazon Elastic Container Service (ECS), simplifying your development to production workflow. Amazon ECR eliminates the need to operate your own container repositories or worry about scaling the underlying infrastructure. Amazon ECR hosts your images in a highly available and scalable architecture, allowing you to reliably deploy containers for your applications. Integration with AWS Identity and Access Management (IAM) provides resource-level control of each repository. For more information, see https://aws.amazon.com/ecr/.
{{% /notice %}}

1. Type in __mybatchapp__ as the repository name. Click the button __Create repository__

1. Select the repository __mybatchapp__ and click the button __View push commands__

1. Make sure you select the __Windows__ tab and copy/paste the commands 1, 3 and 4 in your powershell window on the lab machine. We skip command #2 because in this lab we didn't use a Dockerfile to create the image.

1. Go back to the repository and verify there is now an image with the _latest_ tag.

### Create an ECS cluster

{{% notice info %}} <b>What is Amazon ECS?</b>
Amazon Elastic Container Service (Amazon ECS) is a highly scalable, high-performance container orchestration service that supports Docker containers and allows you to easily run and scale containerized applications on AWS. Amazon ECS eliminates the need for you to install and operate your own container orchestration software, manage and scale a cluster of virtual machines, or schedule containers on those virtual machines. For more information, see https://aws.amazon.com/ecs/.
{{% /notice %}}

__Optional__:
For more details, please watch the video https://www.youtube.com/watch?v=ODF0-HCzlKk

1. Go to the [Amazon ECS](https://us-east-1.console.aws.amazon.com/ecs/home?region=us-east-1#/clusters) console and click on ***Create Cluster***


1. Select __EC2 Windows + Networking__ and click __Next step__

1. Pick a name for your cluster such as __MyContosoCluster__ and use the default values except for the following fields:

    - __EC2 Instance type__: t2.large

    - __Number of instances__: 2

    - __VPC__: the contoso-vpc-xxxxx (10.0.0.0/16)

    - __Subnets__: both subnets

    - __Security group__: ecsInstances-SG-xxxxx

    - __Container instance IAM role__: ecsInstanceRole-xxxxx

1. Click the __Create__ button, wait a few seconds and the cluster will be created.

1. Open your cluster and click the __ECS Instances__ tab, the EC2 instances will show up when they are ready. It might take a few minutes for the instances to get ready. You can go to the EC2 Instances console to view the instance status. If you don't want to wait, you can continue with the steps to create a task definition.

### Create an ECS task definition

__Optional__:
For more details, please watch the video https://youtu.be/MEKtOWTYMp8


1. Open the [task definitions page](https://console.aws.amazon.com/ecs/home?region=us-east-1#/taskDefinitions) at the Amazon ECS console.

1. Click __Create new task Definition__; select __EC2__ and click __Next step__

1. Provide a name such as __MyBatchAppTask__ and click the button __Add container__ in the Container Definitions section.

1. Provide a name such as __MyBatchAppContainer__ for the Container name

1. Add the image URL you get from the first step, for example: 123456789012.dkr.ecr.us-east-1.amazonaws.com/mybatchapp

1. Change __Memory Limits (MiB)__ to 256

1. Change __CPU units__ to 1024

1. Enable __Auto-configure CloudWatch Logs__ for __Log configuration__

1. Click __Add__

    __Note:__ you don't have to specify a command or an entrypoint since we have already added default entrypoint for the container image.

1. Click __Create__

### Run a task in the cluster

1. Follow the step above, in the __Action__ drop down button, select __Run Task__ 

    __Note:__ you can also run a task from the _tasks tab_ in the cluster page.

1. Select __Launch Type__ as __EC2__ and click __Run Task__

1. Wait a little bit and you can see the task changes from __Pending__ to __Running__

    __Note:__ you will have an error if the instance is not ready for the cluster.

1. Click into the task and you can view the output from the __Logs__ tab or expand the task and click __View logs in CloudWatch__

1. Stop the task by selecting the task and clicking the __Stop__ button under the __Tasks__ tab.

