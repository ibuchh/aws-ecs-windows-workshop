+++
title = "CI/CD with ECS (Optional)"
weight = 35
chapter = true
pre = "<b>6. </b>"
+++
<br />
There are multiple ways to do CI/CD for a containerized application. For more information about CI/CD with AWS native tools, you can take a look the following tutorials:

[Tutorial: Continuous Deployment with CodePipeline](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-cd-pipeline.html)

[Tutorial: Create a Pipeline with an Amazon ECR Source and ECS-to-CodeDeploy Deployment](https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-ecs-ecr-codedeploy.html#tutorials-ecs-ecr-codedeploy-taskdefinition)


In this module, we will configure a simple pipeline in AWS CodePipeline that deploys a container application to ECS when there is an update of the image.

{{% notice info %}} <b>What is AWS CodePipeline?</b>
AWS CodePipeline is a continuous delivery service that enables you to model, visualize, and automate the steps required to release your software. For more information, see https://docs.aws.amazon.com/codepipeline/latest/userguide/welcome.html.
{{% /notice %}}


### Create a configuration repository in CodeCommit

1. First, let's get the AWS account id since we need it in a later step. In a powershell window on the lab machine, run the following command and make a note of the __Account__:

```bash
aws sts get-caller-identity
```

1. Open the [CodeCommit console](https://console.aws.amazon.com/codesuite/codecommit/repositories?region=us-east-1)

1. Click __Create repository__

1. Provide a name such as __MyWebAppTask-Config__ and click the button __Create__

1. Click the button __Create file__ at the bottom of the page.

1. Copy/paste the following to the file editor and replace __AWS account__ with the value you got from the first step:	

```
[{"name":"mywebapp","imageUri":"{AWS account}.dkr.ecr.us-east-1.amazonaws.com/mywebapp:latest"}]
```

1. Use __imagedefinitions.json__ as the file name

1. Fill in the __Author name__ and __Eamil address__. You can use any value you want.

1. Click the button __Commit changes__

### Create a pipeline in CodePipeline

1. Open the [CodePipeline console](https://console.aws.amazon.com/codesuite/codepipeline/pipelines?region=us-east-1

1. Click __Create pipeline__ and name it __webapp-pipeline__

1. Click __Next__ and select __AWS CodeCommit__ as the __Source provider__

1. Select __MyWebAppTask-Config__ as the repository name and __master__ as the branch name.

1. Click __Next__ and __Skip build stage__

1. In this lab, we don't build the image from the source, so click the button __Skip__

1. Select __Amazon ECS__ as the deploy provider.

1. Select __MyContosoCluster__ as the cluster name and __mywebapp__ as the service name.

1. Use __imagedefinitions.json__ in the __Image definitions file__ and click __Next__

1. The pipeline will be created and immediately start a deployment.

### Add the ECR repoistory to the __Source__ stage

1. Click the __Edit__ button at the top of the webapp-pipeline.

1. Click __Edit stage__ of __Source__ and click __Add action__

1. Give a name to the __Action name__ filed such as __Image__

1. Select __Amazon ECR__ as the action provider and use the following values for the page:

	- __Repository name__: mywebapp
	- __Output artifacts__: MyImage

1. Click __Done__ then save the change.


### Make a change in the source code and push a new image version

1. On the lab machine, we can make a minor change in the __index.cshtml__ under __MyWebApp/Views__

1. For example, change the sentence inside the __h1__ tag to __Welcome to ECS__

1. Right click __docker-compose__ and click __Rebuild__

1. Tag and push the image, for example (replace the account id to yours):

```
Invoke-Expression -Command (Get-ECRLoginCommand -Region us-east-1).Command

docker tag mywebapp:latest {account-id}.dkr.ecr.us-east-1.amazonaws.com/mywebapp:latest

docker push {account-id}.dkr.ecr.us-east-1.amazonaws.com/mywebapp:latest
```

1. Go back to the pipleline and a new deployment will start shortly.

1. You can go to the ECS cluster to view the deployment events. Once the deployment is complete, you can refresh the web page to view the change.