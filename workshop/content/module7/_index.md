+++
title = "Conclusions"
weight = 65
chapter = true
pre = "<b>7. </b>"
+++
<br />

### Congratulations! You've made it through the workshop. Let's look back through what you accomplished...

#### You used the docker CLI created and updated container images.

#### You containerized a simple CLI application and ran it as an ECS task.

#### You containerized an ASP.net web application and ran it as an ECS service behind a load balancer.


### Want to explore more? Check out these other useful resources:

* [Containerize Legacy .NET Framework Web Apps for Cloud Migration](https://www.youtube.com/watch?v=iu9ei66_KLo)

* [Thomson Reuters: How It Hosted NET App on ECS Using Windows Containers](https://www.youtube.com/watch?v=75p2ete1Cqo)

* [Migrate & Modernize Legacy Microsoft Applications with Containers](https://www.youtube.com/watch?v=ZSQ5zOi3lx8)

* [Migrate Microsoft Applications to AWS like an Expert](https://www.youtube.com/watch?v=szX7rgq_4VM)

* [How to Containerise .NET Apps and Run Them on AWS](https://www.youtube.com/watch?v=wagoy7GYXSM)
