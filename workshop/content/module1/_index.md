+++
title = "Lab environment"
weight = 5
chapter = true
pre = "<b>1. </b>"
+++
<br />
### AWS Management Console

This workshop requires an AWS acccount. You will need the **Participant Hash** provided upon entry.
Connect to the Event Engine portal by clicking the link below or browsing to [https://dashboard.eventengine.run/](https://dashboard.eventengine.run/).

Type in the hash code you received from AWS.

__Note:__ The code and instructions in this workshop assume only one participant is using a given AWS account at a time. If you attempt sharing an account with another participant, you may encounter naming conflicts for certain resources. You can work around this by using distinct regions, but the instructions do not provide details on the changes required to make this work.

Click the button __AWS Console__, then __Open AWS Console__.

Make sure use the __Northern Virginia region (us-east-1)__ for this workshop.


## Provision the AWS resources required for this workshop

To complete the rest of the modules in this workshop, we will need several resources:

* A Virtual Private Cloud (VPC) to act as our networking environment.
* A Windows EC2 instance environment as our development environment (IDE).
* An ECS cluster to run the containers


A CloudFormation stack has already been deployed, which provisioned the following resources:

* A **VPC** with an internet gateway and 2 public subnets.
* A **Windows EC2 instance** which has the devlopment tooling installed.

![initial resources diagram](../screenshots/architecture0.png)
