+++
title = "Run a web application on ECS"
weight = 25
chapter = true
pre = "<b>5. </b>"
+++
<br />
In this module, we will create a ECS service to run the web application. The service is load balanced by an application load balancer.

__Optional__:
For more details, please watch the video https://youtu.be/R72a2hVBel0

### Create an ECS task definition


1. Open the [task definitions page](https://console.aws.amazon.com/ecs/home?region=us-east-1#/taskDefinitions) at the Amazon ECS console.

1. Click __Create new task Definition__; select __EC2__ and click __Next step__

1. Provide a name such as __MyWebAppTask__ and click the button __Add container__ in the Container Definitions section.

1. Provide a name such as __MyWebAppContainer__ for the Container name

1. Add the image URL for the web app repository (you get it from the ECR page), for example: 123456789012.dkr.ecr.us-east-1.amazonaws.com/mywebapp

1. Change __Memory Limits (MiB)__ to 1024

1. In the __Port mappings__ section, change the _Host port_ to __0__, _Container port_ to __80__

1. Change __CPU units__ to 1024

1. Enable __Auto-configure CloudWatch Logs__ for __Log configuration__

1. Click __Add__

    __Note:__ you don't have to specify a command or an entrypoint since we have already added default entrypoint for the container image.

1. Click __Create__


### Create an application load balancer

1. Navigate to the EC2 [Load Balancers](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LoadBalancers:) page.

1. Click __Create Load Balancer__ and click __Create__ under __Application Load Balancer__

1. Give a name such as __MyWebAppLB__. Make sure you select the __contoso-vpc-xxx__ VPC and check both subnets.

1. Click __Next__, then __Next__ again.

1. Add the security group __LoadBalancer-SG-xxx__ and click __Next: Configure Routing__

1. Since we are going to manage the routing from ECS, the target group here will not be used. We can add a dummy one by giving a name such as _dummy_

1. Click __Next__, __Next__ and __Create__

While AWS is provisioning the ALB, you can move to the next step.

### Create a service for the web application

1. Go back to our ECS cluster, undert the __Services__ tab, click __Create__

1. Configure the service as following:

	![screenshot](../screenshots/service-config.png)
	

1. Click __Next step__

1. Select __Application Load Balancer__ in the __Load balancing__ section.

1. Select the ALB we just created.

1. Click __Add to load balancer__ and set the fields as following:

	![screenshot](../screenshots/alb-config.png)

1. Uncheck __Enable service discovery integration__ since we don't plan to use service discovery.

1. Click __Next step__

1. Click __Next step__

1. Click __Create service__

1. Click __View service__ and we will be redirect to the tasks tab. After a short while, you can see new tasks showing up with __PENDING__ status.

1. Once the tasks for the serivce in a __RUNNING__ status, you can verify if they are healthy for the ALB. Open the [target group page](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#TargetGroups:sort=targetGroupName), select the ecs-xxx target group, and click the __Targets__ tab.

1. Wait until the status becomes __healthy__ for the registered targets.

1. Go back to the [ALB](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LoadBalancers:sort=loadBalancerName), and find its __DNS name__

1. Copy/paste the DNS name to your browser and it should open the web app.