+++
title = "Containerize a web application "
date = 2019-10-01T21:30:10-04:00
weight = 20
chapter = true
pre = "<b>4. </b>"
+++
<br />
### Containerize an ASP.NET web application

__Optional__:
For more details, please watch the video https://youtu.be/M7Hoeb4DI-E

1. On the lab machine, open __Visual Studio 2019__(a shortcut on the desktop)

1. Open __MyWebApp.sln__ (If it's not on the recent tab, you can find the solution under C:\Users\Administrator\Downloads\assets\300_build_website_container\MyWebApp)

1. Inspect the __Dockerfile__ and the __docker-compose.yml__ file. They will show you how we build the container.

1. Right click __docker-compose__ on the right side panel, you can select __Debug__ and click __Start new instance__. It will run the container for you and bring up the default browser. You should see a page like the following:

    ![screenshot](../screenshots/web-app.png)

1. Run __docker images__ in a powershell window, you can see there is an image __mywebapp__ with a __dev__ tag.

1. In the top toolbar, stop the debug session and change __Debug__ to __Release__ in the _solutions configuration_ drop down list.

1. Right click __docker-compose__ again and click build.

1. Run __docker images__ again and you will see __mywebapp__ now has a __latest__ tag.

### Create a image repository for the web application

1. On the lab machine, run the following command and make a note of the __repositoryUri__

```bash
aws ecr create-repository --repository-name mywebapp --region us-east-1
```

1. Login to ECR by running the following command:

```powershell
Invoke-Expression -Command (Get-ECRLoginCommand -Region us-east-1).Command
```

1. Tag and push the image by running the following commands. You can find the __repositoryUri__ value from the output of the first command (output of __aws ecr create-repository__ ...).

```bash
docker tag mywebapp:latest {repositoryUri}:latest

docker push {repositoryUri}:latest
```
